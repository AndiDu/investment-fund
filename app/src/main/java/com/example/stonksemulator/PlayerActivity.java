package com.example.stonksemulator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PlayerActivity extends AppCompatActivity {

    int money;
    int startMoney;
    int month;
    ArrayList<Buyable> buyables;
    public static ArrayList<Map<String, Integer>> history;
    public static ArrayList<String> names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            money = arguments.getInt("money");
        } else {
            money = 1000;
        }

        startMoney = money;
        month = 0;
        names = new ArrayList<>();
        history = new ArrayList<>();

        Random random = new Random(System.currentTimeMillis());

        buyables = new ArrayList<>();

        Buyable itStock = new Exchanging("IT Industry", R.drawable.ic_laptop_24, 400, 14, 8,
                findViewById(R.id.textViewStock1BuySellPrice), findViewById(R.id.textViewStock1Amount), random);
        names.add(itStock.getName());
        Buyable gasStock = new Exchanging("Oil Industry", R.drawable.ic_gas_24, 75, 25, 20,
                findViewById(R.id.textViewStock2BuySellPrice), findViewById(R.id.textViewStock2Amount), random);
        names.add(gasStock.getName());
        Buyable flyStock = new Exchanging("Transport Industry", R.drawable.ic_airplane_24, 80, 11, 4,
                findViewById(R.id.textViewStock3BuySellPrice), findViewById(R.id.textViewStock3Amount), random);
        names.add(flyStock.getName());
        Buyable foodStock = new Exchanging("Food Industry", R.drawable.ic_fastfood_24, 100, 9, 3,
                findViewById(R.id.textViewStock4BuySellPrice), findViewById(R.id.textViewStock4Amount), random);
        names.add(foodStock.getName());

        Buyable euroBonds = new Exchanging("Euro Bonds", R.drawable.ic_euro_24, 200, 2, 1,
                findViewById(R.id.textViewBond1BuySellPrice), findViewById(R.id.textViewBond1Amount), random);
        names.add(euroBonds.getName());
        Buyable dollarBonds = new Exchanging("Dollar Bonds", R.drawable.ic_dollar_24, 200, 3, 2,
                findViewById(R.id.textViewBond2BuySellPrice), findViewById(R.id.textViewBond2Amount), random);
        names.add(dollarBonds.getName());
        Buyable rubleBonds = new Exchanging("Ruble Bonds", R.drawable.ic_round_money_24, 200, 5, 5,
                findViewById(R.id.textViewBond3BuySellPrice), findViewById(R.id.textViewBond3Amount), random);
        names.add(rubleBonds.getName());

        Buyable gold = new Exchanging("Gold", R.drawable.ic_gold_24, 250, 1, 0,
                findViewById(R.id.textViewMetal1BuySellPrice), findViewById(R.id.textViewMetal1Amount), random);
        names.add(gold.getName());
        Buyable silver = new Exchanging("Silver", R.drawable.ic_silver_24, 50, 1, 0,
                findViewById(R.id.textViewMetal2BuySellPrice), findViewById(R.id.textViewMetal2Amount), random);
        names.add(silver.getName());
        Buyable platinum = new Exchanging("Platinum", R.drawable.ic_platinum_24, 200, 1, 0,
                findViewById(R.id.textViewMetal3BuySellPrice), findViewById(R.id.textViewMetal3Amount), random);
        names.add(platinum.getName());

        Buyable euroDeposit = new Accumulating("Euro Deposit", R.drawable.ic_euro_24, 200, 3, 0, 6,
                findViewById(R.id.textViewDeposit1Amount), random);
        names.add(euroDeposit.getName());
        Buyable dollarDeposit = new Accumulating("Dollar Deposit", R.drawable.ic_dollar_24, 200, 3, 0, 6,
                findViewById(R.id.textViewDeposit2Amount), random);
        names.add(dollarDeposit.getName());
        Buyable rubbleDeposit = new Accumulating("Ruble Deposit", R.drawable.ic_round_money_24, 200, 3, -1, 5,
                findViewById(R.id.textViewDeposit3Amount), random);
        names.add(rubbleDeposit.getName());
        buyables.add(itStock);
        buyables.add(gasStock);
        buyables.add(flyStock);
        buyables.add(foodStock);

        buyables.add(gold);
        buyables.add(silver);
        buyables.add(platinum);

        buyables.add(euroBonds);
        buyables.add(dollarBonds);
        buyables.add(rubleBonds);

        buyables.add(euroDeposit);
        buyables.add(dollarDeposit);
        buyables.add(rubbleDeposit);

        ConstraintLayout constraintLayout = findViewById(R.id.layoutStock1);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(itStock);
            }
        });

        constraintLayout = findViewById(R.id.layoutStock2);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(gasStock);
            }
        });

        constraintLayout = findViewById(R.id.layoutStock3);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(flyStock);
            }
        });

        constraintLayout = findViewById(R.id.layoutStock4);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(foodStock);
            }
        });

        constraintLayout = findViewById(R.id.layoutDeposit1);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(euroDeposit);
            }
        });

        constraintLayout = findViewById(R.id.layoutDeposit2);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(dollarDeposit);
            }
        });

        constraintLayout = findViewById(R.id.layoutDeposit3);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(rubbleDeposit);
            }
        });

        constraintLayout = findViewById(R.id.layoutMetal1);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(gold);
            }
        });

        constraintLayout = findViewById(R.id.layoutMetal2);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(silver);
            }
        });

        constraintLayout = findViewById(R.id.layoutMetal3);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(platinum);
            }
        });


        constraintLayout = findViewById(R.id.layoutBond1);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(euroBonds);
            }
        });

        constraintLayout = findViewById(R.id.layoutBond2);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(dollarBonds);
            }
        });

        constraintLayout = findViewById(R.id.layoutBond3);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                setTextsAndImagesToSelectedItemLayout(rubleBonds);
            }
        });

        for (Buyable b : buyables) {
            b.setPrice();
        }
        TextView textViewMoneyValue = findViewById(R.id.textViewMoneyValue);
        textViewMoneyValue.setText("" + money);
        countAndSetTotal();
        deselect();

        writeToHistory();
    }

    @SuppressLint("SetTextI18n")
    private void setTextsAndImagesToSelectedItemLayout(Buyable buyable) {
        TextView textView = findViewById(R.id.textViewSelectedItemBuyValue);
        textView.setText("" + buyable.getBuyPrice());
        textView = findViewById(R.id.textViewSelectedItemSellValue);
        textView.setText("" + buyable.getSellPrice());
        textView = findViewById(R.id.textViewSelectedItemName);
        textView.setText(buyable.getName());
        textView = findViewById(R.id.textViewSelectedItemRiskValue);
        textView.setText(buyable.getRisk());
        textView = findViewById(R.id.textViewSelectedItemProfitValue);
        textView.setText(buyable.getProfit());

        ImageView imageView = findViewById(R.id.imageViewSelectedItem);
        imageView.setImageResource(buyable.getDrawableId());

        ConstraintLayout constraintLayout = findViewById(R.id.layoutSelectedItemBuy);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int result = buyable.buy(money);
                if (result > 0) {
                    money = result;
                } else {
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setText("Not enough money");
                    toast.show();
                }
                TextView textViewMoneyValue = findViewById(R.id.textViewMoneyValue);
                textViewMoneyValue.setText("" + money);
                TextView textViewSelectedItemSellValue = findViewById(R.id.textViewSelectedItemSellValue);
                textViewSelectedItemSellValue.setText("" + buyable.getSellPrice());
                countAndSetTotal();
            }
        });
        constraintLayout = findViewById(R.id.layoutSelectedItemSell);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int result = buyable.sell(money);
                if (result > 0) {
                    money = result;
                } else {
                    Toast toast = new Toast(getApplicationContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setText("Nothing to sell");
                    toast.show();
                }
                TextView textViewMoneyValue = findViewById(R.id.textViewMoneyValue);
                textViewMoneyValue.setText("" + money);
                TextView textViewSelectedItemSellValue = findViewById(R.id.textViewSelectedItemSellValue);
                textViewSelectedItemSellValue.setText("" + buyable.getSellPrice());
                countAndSetTotal();
            }
        });

    }

    public void onClickNextMonth(View view) {
        writeToHistory();
        for (Buyable b : buyables) {
            b.nextMonth();
        }
        countAndSetTotal();
        TextView textViewMonthValue = findViewById(R.id.textViewMonthValue);
        month++;
        textViewMonthValue.setText("" + month);

        deselect();
    }

    private void deselect() {
        ImageView imageView = findViewById(R.id.imageViewSelectedItem);
        imageView.setImageResource(R.drawable.ic_round_image_24);

        ConstraintLayout constraintLayout = findViewById(R.id.layoutSelectedItemSell);
        constraintLayout.setOnClickListener(null);
        constraintLayout = findViewById(R.id.layoutSelectedItemBuy);
        constraintLayout.setOnClickListener(null);

        TextView textView = findViewById(R.id.textViewSelectedItemName);
        textView.setText("Select an asset");
        textView = findViewById(R.id.textViewSelectedItemRiskValue);
        textView.setText("?");
        textView = findViewById(R.id.textViewSelectedItemProfitValue);
        textView.setText("?");
        textView = findViewById(R.id.textViewSelectedItemBuyValue);
        textView.setText("?");
        textView = findViewById(R.id.textViewSelectedItemSellValue);
        textView.setText("?");
    }

    @SuppressLint("SetTextI18n")
    private void countAndSetTotal() {
        int total = getTotal();
        TextView textView = findViewById(R.id.textViewTotalValue);
        textView.setText("" + total);

        int profit = total - startMoney;
        float percentage = (float) (profit * 100. / startMoney);

        textView = findViewById(R.id.textViewProfitValueAmount);
        textView.setText("" + profit);

        textView = findViewById(R.id.textViewProfitValuePercent);
        textView.setText(String.format("(%.2f%%)", percentage));

    }

    private int getTotal() {
        int total = money;
        for (Buyable b : buyables) {
            total += b.getTotal();
        }
        return total;
    }

    private void writeToHistory() {
        Map<String, Integer> map = new HashMap<>();
        map.put("Number", month);
        for (Buyable b : buyables) {
            map.put(b.getName(), b.getTotal());
        }
        map.put("Money", money);
        map.put("Total", getTotal());
        history.add(map);
    }

    public void onClickFinish(View view) {
        writeToHistory();
        Intent intent = new Intent(PlayerActivity.this, ResultActivity.class);
        startActivity(intent);
    }
}