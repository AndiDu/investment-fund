package com.example.stonksemulator;

import android.annotation.SuppressLint;
import android.widget.TextView;

import java.util.Random;

public class Exchanging implements Buyable {
    protected int price;
    private int profit;
    private int risk;
    private int amount;
    private TextView textViewPriceInfo;
    private TextView textViewAmountInfo;
    private String name;
    private int drawableId;

    Random random;

    public Exchanging(String name, int drawableId, int price, int profit, int risk, TextView textViewPriceInfo, TextView textViewAmountInfo, Random random) {
        this.price = price;
        this.profit = profit;
        this.risk = risk;
        this.textViewAmountInfo = textViewAmountInfo;
        this.textViewPriceInfo = textViewPriceInfo;
        amount = 0;
        this.name = name;
        this.drawableId = drawableId;
        this.random = random;
    }

    public void setPrice() {
        textViewPriceInfo.setText(getAllPrice());
    }

    @SuppressLint("SetTextI18n")
    private void setAmount() {
        textViewAmountInfo.setText("" + amount);
    }

    public int getBuyPrice() {
        return (int) (price + Math.round(price / 100.));
    }

    public int getSellPrice() {
        return (int) (price - Math.round(price / 100.));
    }

    public void nextMonth() {
        int growth;
        if (risk != 0) {
            growth = random.nextInt(profit) - random.nextInt(risk);
        } else {
            growth = profit;
        }
        price = (int) Math.round(price * ((100 + growth) / 100.));

        setPrice();
    }

    public int buy(int money) {
        int buyPrice = getBuyPrice();
        if (money < buyPrice) {
            return -1;
        } else {
            amount++;
            setAmount();
            return money - buyPrice;
        }
    }

    public int sell(int money) {
        int sellPrice = getSellPrice();
        if (amount < 1) {
            return -1;
        } else {
            amount--;
            setAmount();
            return money + sellPrice;
        }
    }

    public String getAllPrice() {
        return getBuyPrice() + " / " + getSellPrice();
    }

    public int getTotal() {
        return price * amount;
    }

    public String getProfit() {
        return profit + "%";
    }

    public String getRisk() {
        if (risk == 0) {
            return "-";
        }
        return risk + "%";
    }

    public String getName() {
        return name;
    }

    public int getDrawableId() {
        return drawableId;
    }
}
