package com.example.stonksemulator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

public class ResultActivity extends AppCompatActivity {

    ArrayList<Integer> textViews = new ArrayList<Integer>() {};
    int start = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textViews.add(R.id.textView1);
        textViews.add(R.id.textView2);
        textViews.add(R.id.textView3);
        textViews.add(R.id.textView4);
        textViews.add(R.id.textView5);
        textViews.add(R.id.textView6);
        textViews.add(R.id.textView7);
        textViews.add(R.id.textView8);
        textViews.add(R.id.textView9);
        textViews.add(R.id.textView10);
        textViews.add(R.id.textView11);
        textViews.add(R.id.textView12);
        textViews.add(R.id.textView13);

        int j = 0;
        for (String name: PlayerActivity.names) {
            TextView textView = findViewById(textViews.get(j));
            textView.setText(name);
            j++;
        }

        TextView textView = findViewById(R.id.textView0);
        textView.setText("Month");
        textView = findViewById(R.id.textView14);
        textView.setText("Money");
        textView = findViewById(R.id.textView15);
        textView.setText("Total");

        int money, total = 0, number;

        int counter = 0;
        for (Map map : PlayerActivity.history) {
            LinearLayout linearLayout = findViewById(R.id.resultLayout);
            LayoutInflater inflater = getLayoutInflater();
            View layer = inflater.inflate(R.layout.result_line, null);

            textView = layer.findViewById(R.id.textView0);
            if (counter == 0) {
                textView.setText("Start");
            } else {
                number = (int) map.get("Number");
                textView.setText(number + "");
            }

            int i = 0;
            for (String name : PlayerActivity.names) {
                textView = layer.findViewById(textViews.get(i));
                textView.setText(map.get(name) + "");
                i++;
            }
            textView = layer.findViewById(R.id.textView14);
            money = (int) map.get("Money");
            textView.setText(money + "");
            textView = layer.findViewById(R.id.textView15);
            if (start < 0) {
                start = (int) map.get("Total");
            }
            total = (int) map.get("Total");
            textView.setText(total + "");

            linearLayout.addView(layer);
            counter++;
        }

        textView = findViewById(R.id.textViewProfitBeforeTaxValue);
        textView.setText(total + "");

        textView = findViewById(R.id.textViewAfterTaxValue);
        textView.setText(String.format("%.2f", total - (total - start) * 0.17));
    }
}