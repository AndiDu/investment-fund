package com.example.stonksemulator;

import android.annotation.SuppressLint;
import android.widget.TextView;

import java.util.Random;

public class Accumulating implements Buyable {
    private int price;
    private int startPrice;
    private int profit;
    private int lowestProfit, highestProfit;
    private TextView textViewPriceInfo;
    private String name;
    private int drawableId;

    Random random;

    public Accumulating(String name, int drawableId, int startPrice, int profit, int lowestProfit, int highestProfit, TextView textViewPriceInfo, Random random) {
        this.price = 0;
        this.startPrice = startPrice;
        this.profit = profit;
        this.highestProfit = highestProfit;
        this.lowestProfit = lowestProfit;
        this.textViewPriceInfo = textViewPriceInfo;
        this.name = name;
        this.drawableId = drawableId;
        this.random = random;
    }

    public void setPrice() {
        textViewPriceInfo.setText(getAllPrice());
    }

    public int getBuyPrice() {
        return startPrice;
    }

    public int getSellPrice() {
        return price;
    }

    public void nextMonth() {
        price = (int) Math.round(price * ((100 + profit) / 100.));
        setPrice();

        if (profit <= lowestProfit) {
            profit++;
        } else if (profit >= highestProfit) {
            profit--;
        } else {
            profit += random.nextInt(3) - 1;
        }
    }

    public int buy(int money) {
        if (money < startPrice) {
            return -1;
        } else {
            price += startPrice;
            setPrice();
            return money - startPrice;
        }
    }

    public int sell(int money) {
        int sellPrice = getSellPrice();
        if (price == 0) {
            return -1;
        } else {
            int tmp = price;
            price = 0;
            setPrice();
            return money + tmp;
        }
    }

    public String getAllPrice() {
        return startPrice + " / " + price;
    }

    public int getTotal() {
        return price;
    }

    public String getProfit() {
        return profit + "%";
    }

    public String getRisk() {
        return "-";
    }

    public String getName() {
        return name;
    }

    public int getDrawableId() {
        return drawableId;
    }

}
