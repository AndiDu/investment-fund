package com.example.stonksemulator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
    }

    public void onClickStart(View view) {
        EditText editText = findViewById(R.id.editTextEnterMoney);
        String data = editText.getText().toString();

        int money;
        try {
            money = Integer.parseInt(data);
        } catch (Exception e) {
            money = 1000;
        }
        Intent intent = new Intent(FirstActivity.this, PlayerActivity.class);
        intent.putExtra("money", money);
        startActivity(intent);
    }
}