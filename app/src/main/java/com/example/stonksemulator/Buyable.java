package com.example.stonksemulator;

import android.annotation.SuppressLint;
import android.widget.TextView;

import java.util.Random;

public interface Buyable {

    void setPrice();
    int getBuyPrice();
    int getSellPrice();

    void nextMonth();

    int buy(int money);

    int sell(int money);

    String getAllPrice();

    int getTotal();

    String getProfit();
    String getRisk();

    String getName();

    int getDrawableId();
}
